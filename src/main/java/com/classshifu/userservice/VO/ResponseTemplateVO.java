package com.classshifu.userservice.VO;

import com.classshifu.userservice.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTemplateVO {
    private User user;
    private Department department;

}

// So, Here, I can use ResponseTemplateVO as an return type as my User and Department
