package com.classshifu.userservice.controller;

import com.classshifu.userservice.VO.ResponseTemplateVO;
import com.classshifu.userservice.entity.User;
import com.classshifu.userservice.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {

    // object of userService
    @Autowired
    private UserService userService;

    @PostMapping("/")
    public User saveUser(@RequestBody User user){
        log.info("Inside saveUser of UserController");
        return userService.saveUser(user);
    }

    @GetMapping("/{id}")
    public ResponseTemplateVO getUserWithDepartment(@PathVariable("id")  Long userId){
        log.info("ResponseTemplateVO controller");
        return userService.getUserWithDepartment(userId);
    }




}
