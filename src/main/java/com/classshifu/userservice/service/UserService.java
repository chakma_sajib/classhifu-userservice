package com.classshifu.userservice.service;

import com.classshifu.userservice.VO.Department;
import com.classshifu.userservice.VO.ResponseTemplateVO;
import com.classshifu.userservice.entity.User;
import com.classshifu.userservice.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RestTemplate restTemplate;

    public User saveUser(User user) {
        log.info("User Service -saveUser");
        return userRepository.save(user);
    }

    public ResponseTemplateVO getUserWithDepartment(Long userId) {
        log.info("User Service -calling from Response");
        ResponseTemplateVO vo = new ResponseTemplateVO();
        User user = userRepository.findByUserId(userId);
        // Now, to call Another microservice Department
        Department department =
                restTemplate.getForObject("http://DEPARTMENT-SERVICE/department/" + user.getDepartmentId(), Department.class);

       vo.setUser(user);
       vo.setDepartment(department);
       return vo;

    }
}
